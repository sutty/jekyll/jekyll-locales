# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'jekyll/locales/version'

Gem::Specification.new do |spec|
  spec.name = 'jekyll-locales'
  spec.version = Jekyll::Locales::VERSION
  spec.authors = ['f']
  spec.email = ['f@sutty.nl']

  spec.summary = 'Jekyll plugin for localized sites'
  spec.description = <<~DESCRIPTION
    This is yet another plugin for Jekyll localization.  It creates a
    copy of the site for each language and allows to interlink between
    them.
  DESCRIPTION
  spec.homepage = 'https://0xacab.org/sutty/jekyll/jekyll-locales'
  spec.license  = 'GPL-3.0'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri'] = 'https://0xacab.org/sutty/jekyll/jekyll-locales/blob/master/CHANGELOG.md'

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      f.match(%r{^(test|spec|features)/})
    end
  end

  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'minitest', '~> 5.0'
  spec.add_development_dependency 'rake', '~> 12.0'
  spec.add_development_dependency 'rubocop', '~> 0.74'
end
