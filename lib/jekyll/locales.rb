# frozen_string_literal: true

require 'jekyll/site'
require 'jekyll/locales/site'
require 'jekyll/locales/site_drop'
require 'jekyll/locales/hooks/page_post_init'
require 'jekyll/locales/hooks/post_render'
require 'jekyll/locales/hooks/cleanup'
