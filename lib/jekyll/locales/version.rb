# frozen_string_literal: true

module Jekyll
  module Locales
    VERSION = '0.1.12'
  end
end
