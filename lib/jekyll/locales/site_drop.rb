# frozen_string_literal: true

require 'jekyll/drops/site_drop'

module Jekyll
  module Drops
    # Make i18n available to Liquid as site.i18n
    class SiteDrop
      def i18n
        @i18n ||= @obj.data[@obj.config['lang']]
      end
    end
  end
end
