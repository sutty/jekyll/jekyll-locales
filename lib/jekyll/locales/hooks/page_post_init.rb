# frozen_string_literal: true

Jekyll::Hooks.register :pages, :post_init do |page|
  next unless page.data.dig('locales', page.site.locale)

  page.data.merge! page.data['locales'][page.site.locale]
end
