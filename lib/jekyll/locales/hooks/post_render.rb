# frozen_string_literal: true

Jekyll::Hooks.register :site, :post_render do |site|
  unless site.config['plugins'].include? 'jekyll-linked-posts'
    Jekyll.logger.warn 'Enable jekyll-linked-posts to generate metadata about alternate pages'
    next
  end

  # URLs can be relative but Google asks for fully qualified URLs
  # @see {https://support.google.com/webmasters/answer/189077?hl=en}
  url = site.config['url']&.sub(%r{/\z}, '')

  unless url
    Jekyll.logger.warn 'Provide a `url` value in configuration to create fully qualified URLs'
    next
  end

  site.documents.each do |doc|
    next if doc.output.nil?
    next unless doc&.data&.dig 'locales'

    alternate = doc.data['locales'].map do |translation|
      locale = translation.collection.label
      # TODO: Every doc should know how to build their own URL
      doc_url = [url, translation.url].join('/')

      %(<link rel="alternate" hreflang="#{locale}" href="#{doc_url}" />)
    end

    next if alternate.empty?

    doc.output.sub! '<head>', '<head>' + alternate.join
  end
end
