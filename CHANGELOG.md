# Changelog

## 0.1.13

- Alternate URLs were adding the locale innecessarily!

## 0.1.12

- Reset and setup every site so generators like jekyll-archive are
  reloaded.  Otherwise we may have data copied over from the previous
  locale.

## 0.1.11

- Add alternate URLs to link between languages when using
  jekyll-linked-posts

## 0.1.10

- Prevent cleanup from removing other languages

## 0.1.9

- Loading locales as collection makes Jekyll render them even when
  they're not part of output.  This version prevents that.  A site with
  three languages was taking 160 seconds before and 60 after patching!

## 0.1.8

- Load other locales as collections to make them available in templates.
  Compatible with
  [jekyll-linked-posts](https://rubygems.org/gems/jekyll-linked-posts).

## 0.1.0

- Localized Jekyll sites
