# Localization for Jekyll

This is yet another plugin for Jekyll localization.  It creates a copy
of the site for each language and allows to interlink between them.

## Installation

Add this line to your application's Gemfile:

```ruby
group :jekyll_plugins do
  gem 'jekyll-locales'
end
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-locales

## Usage

### Configuration

Add your locales into the `_config.yml` file, they can be anything:

```yaml
locales:
- es
- en
- ahr
- brasileiro
redirect: true
```

**The first locale is going to be your default locale**.  If you use
a redirector.html, it will be the locale it will redirector to.  If you
don't use a redirector, the default locale will be at the root of the
site.

## Posts

Put your posts into directories named after the locales, prefixed by an
underscore.  So for `es`, you'll need to create a `_es` directory.

Move everything under `_posts` to the corresponding directory and remove
it.  You can add it to `.gitignore` also.

To link between articles, add a hash of locales and permalinks into each
post front matter:

```yaml
---
locales:
  es: /es/hola-mundo/
  en: /en/hello-world/
  ahr: /ahr/ahoy-matey/
---

```

Then you can access them by using `page.locales` in your layout.

**Tip:** You can use
[jekyll-linked-posts](https://rubygems.org/gems/jekyll-linked-posts) to
make this process more resilient.  It uses IDs instead of URLs and it
also allows you to access other data.  From v0.1.11 it will also
generate alternate URLs so search engines can link between translations.

### Pages

You can translate the front matter by adding locale-specific variables
in it:

```yaml
---
layout: page
title: Default title
locales:
  es:
    title: Título en castellano
    permalink: pagina/
  en:
    title: English title
    permalink: page/
---
```

### Themes

To translate your theme, put the string into `_data/LANGUAGE.yml` files,
where `LANGUAGE` is the name of a locale (ie. for `es`, there's a
corresponding file on `_data/es.yml`).

```yaml
---
lang: Castellano
```

Please note this isn't magically supported by Jekyll themes, so you may
need to modify your layouts.

You can then access your translations in layouts and includes like this:

```liquid
{% assign locale = site.data[site.lang].lang %}
<h1>{{ locale.lang }}</h1>
```

`site.lang` and `site.locale` are configuration keys added by this
plugin and contain the current locale.

**We're adding support for a `locale` variable so you don't have to do
this but we're still working out the implementation details.**

### Site

Once you generate the site, you'll find your `_site` dir (or any other
destination you use) contains a directory per locale, so you have a
localized copy of it.

If you create a file called `redirector.html`, this plugin will copy it
as `index.html` for `_site`, so you can have a welcome page where every
language available is listed, or you may redirect to the default
language:

```liquid
<html>
  <head>
    <!-- Redirect after 10 seconds -->
    <meta http-equiv="refresh" content="10; {{ site.default_locale }}/" />
  </head>
  <body>
    <ul>
      <!-- Let visitors decide which locale they like to access -->
      {% for locale in site.i18n %}
        <li><a href="{{ locale }}/">{{ site.data[locale].lang }}</a></li>
      {% endfor %}
    </ul>
  </body>
</html>
```

## Development

After checking out the repo, run `bin/setup` to install dependencies.
Then, run `rake test` to run the tests. You can also run `bin/console`
for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake
install`. To release a new version, update the version number in
`version.rb`, and then run `bundle exec rake release`, which will create
a git tag for the version, push git commits and tags, and push the
`.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/jekyll-locales>. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.neocities.org/en/code-of-conduct).

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the jekyll-locales project’s codebases, issue
trackers, chat rooms and mailing lists is expected to follow the [code
of conduct](https://sutty.neocities.org/en/code-of-conduct).
